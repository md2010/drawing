﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Draw_LV7
{
    public partial class Form1 : Form
    {
        Graphics g;
        List<Pen> pens = new List<Pen>();
        public bool resized = false;
        public int resize;
        
        public Form1()
        {
            InitializeComponent();
            g = pbDrawing.CreateGraphics();
            pens.Add(new Pen(Color.Black, 1.0F));
            pens.Add(new Pen(Color.Black, 1.0F));
            pens.Add(new Pen(Color.Black, 1.0F));
            pens.Add(new Pen(Color.Black, 1.0F));
        }
  
        private void pbDrawing_MouseUp(object sender, MouseEventArgs e)
        {
            Pen pen;
            int i = 0;
            foreach (RadioButton button in groupBox_shapes.Controls)
            {
                if (button.Checked)
                {

                    if (i == 0)
                    {
                        pens[0].Color = Color.Red;
                    }
                    if (i == 1)
                    {
                        pens[1].Color = Color.Black;
                    }
                    if (i == 2)
                    {
                        pens[2].Color = Color.Green;
                    }
                    if (i == 3)
                    {
                        pens[3].Color = Color.Blue;
                    }
                    break;
                }
                i++;
            } pen = pens[i];

            if (radioButton6_circle.Checked)
            {
                Circle c = new Circle();
                c.draw(g, pen, e.X, e.Y);
            }
            if (radioButton7_Triangle.Checked)
            {
                Triangle t = new Triangle();
                t.draw(g, pen, e.X, e.Y);
            }
            if (radioButton5_line.Checked)
            {
                Line l = new Line();
                l.draw(g, pen, e.X, e.Y);
            }
            if (radioButton_rectangle.Checked)
            {
                Rectangle r = new Rectangle();               
                r.draw(g, pen, e.X, e.Y);
            }
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            int velicina = hScrollBar1.Value;
            lbVelicina.Text = velicina.ToString() + ".00cm";
            pens[0].Width = velicina;
            pens[1].Width = velicina;
            pens[2].Width = velicina;
            pens[3].Width = velicina;
        }       
    }
    
    class Circle : IDrawable
    {
        int r;
        public Circle()
        {                
            r = 10;
        }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawEllipse(p, x, y, r, r);
        }
    }
}

    class Line : IDrawable
    {        
            public Line() {}
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawLine(p, x/2, y, x, y);
        }

}
    class Rectangle : IDrawable
    {
        int a;
        int b;
        public Rectangle()
        {
            a = 30;
            b = 50;
        }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawRectangle(p, x, y, b, a);
        }
    }

    class Triangle : IDrawable
{
    int x1, x2, y1;
    public Triangle()
    {
        x1 = -20;
        x2 = 20;
        y1 = 60;
    }
    public void draw(Graphics g, Pen p, int x, int y)
    {
        PointF point1 = new PointF(x+x1,y+y1);
        PointF point2 = new PointF(x+x2, y+y1);
        PointF point3 = new PointF(x, y);
        g.DrawLine(p, point1, point2);
        g.DrawLine(p, point1, point3);
        g.DrawLine(p, point3, point2);
    }
}
    class Square : IDrawable
{
    int a;
    public Square() {
        a = 30;
    }
    public void draw(Graphics g, Pen p, int x, int y)
    {
        g.DrawRectangle(p, x, y, a, a);
    }
}
    interface IDrawable
{
     void draw(Graphics g, Pen p, int x, int y);
}
