﻿namespace Draw_LV7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbDrawing = new System.Windows.Forms.PictureBox();
            this.radioButton1_red = new System.Windows.Forms.RadioButton();
            this.radioButton2_black = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton3_green = new System.Windows.Forms.RadioButton();
            this.radioButton4_Blue = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButton5_line = new System.Windows.Forms.RadioButton();
            this.radioButton6_circle = new System.Windows.Forms.RadioButton();
            this.radioButton7_Triangle = new System.Windows.Forms.RadioButton();
            this.radioButton_rectangle = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.lbVelicina = new System.Windows.Forms.Label();
            this.groupBox_shapes = new System.Windows.Forms.GroupBox();
            this.groupBox_colors = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing)).BeginInit();
            this.groupBox_shapes.SuspendLayout();
            this.groupBox_colors.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbDrawing
            // 
            this.pbDrawing.Location = new System.Drawing.Point(0, 1);
            this.pbDrawing.Name = "pbDrawing";
            this.pbDrawing.Size = new System.Drawing.Size(748, 597);
            this.pbDrawing.TabIndex = 0;
            this.pbDrawing.TabStop = false;
            this.pbDrawing.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbDrawing_MouseUp);
            // 
            // radioButton1_red
            // 
            this.radioButton1_red.AutoSize = true;
            this.radioButton1_red.Location = new System.Drawing.Point(0, 12);
            this.radioButton1_red.Name = "radioButton1_red";
            this.radioButton1_red.Size = new System.Drawing.Size(55, 21);
            this.radioButton1_red.TabIndex = 1;
            this.radioButton1_red.TabStop = true;
            this.radioButton1_red.Text = "Red";
            this.radioButton1_red.UseVisualStyleBackColor = true;
            // 
            // radioButton2_black
            // 
            this.radioButton2_black.AutoSize = true;
            this.radioButton2_black.Location = new System.Drawing.Point(0, 39);
            this.radioButton2_black.Name = "radioButton2_black";
            this.radioButton2_black.Size = new System.Drawing.Size(63, 21);
            this.radioButton2_black.TabIndex = 2;
            this.radioButton2_black.TabStop = true;
            this.radioButton2_black.Text = "Black";
            this.radioButton2_black.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(754, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Shape";
            // 
            // radioButton3_green
            // 
            this.radioButton3_green.AutoSize = true;
            this.radioButton3_green.Location = new System.Drawing.Point(0, 66);
            this.radioButton3_green.Name = "radioButton3_green";
            this.radioButton3_green.Size = new System.Drawing.Size(69, 21);
            this.radioButton3_green.TabIndex = 4;
            this.radioButton3_green.TabStop = true;
            this.radioButton3_green.Text = "Green";
            this.radioButton3_green.UseVisualStyleBackColor = true;
            // 
            // radioButton4_Blue
            // 
            this.radioButton4_Blue.AutoSize = true;
            this.radioButton4_Blue.Location = new System.Drawing.Point(0, 93);
            this.radioButton4_Blue.Name = "radioButton4_Blue";
            this.radioButton4_Blue.Size = new System.Drawing.Size(57, 21);
            this.radioButton4_Blue.TabIndex = 5;
            this.radioButton4_Blue.TabStop = true;
            this.radioButton4_Blue.Text = "Blue";
            this.radioButton4_Blue.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(754, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Color";
            // 
            // radioButton5_line
            // 
            this.radioButton5_line.AutoSize = true;
            this.radioButton5_line.Location = new System.Drawing.Point(5, 75);
            this.radioButton5_line.Name = "radioButton5_line";
            this.radioButton5_line.Size = new System.Drawing.Size(56, 21);
            this.radioButton5_line.TabIndex = 7;
            this.radioButton5_line.TabStop = true;
            this.radioButton5_line.Text = "Line";
            this.radioButton5_line.UseVisualStyleBackColor = true;
            // 
            // radioButton6_circle
            // 
            this.radioButton6_circle.AutoSize = true;
            this.radioButton6_circle.Location = new System.Drawing.Point(5, 21);
            this.radioButton6_circle.Name = "radioButton6_circle";
            this.radioButton6_circle.Size = new System.Drawing.Size(64, 21);
            this.radioButton6_circle.TabIndex = 8;
            this.radioButton6_circle.TabStop = true;
            this.radioButton6_circle.Text = "Circle";
            this.radioButton6_circle.UseVisualStyleBackColor = true;
            // 
            // radioButton7_Triangle
            // 
            this.radioButton7_Triangle.AutoSize = true;
            this.radioButton7_Triangle.Location = new System.Drawing.Point(5, 48);
            this.radioButton7_Triangle.Name = "radioButton7_Triangle";
            this.radioButton7_Triangle.Size = new System.Drawing.Size(81, 21);
            this.radioButton7_Triangle.TabIndex = 9;
            this.radioButton7_Triangle.TabStop = true;
            this.radioButton7_Triangle.Text = "Triangle";
            this.radioButton7_Triangle.UseVisualStyleBackColor = true;
            // 
            // radioButton_rectangle
            // 
            this.radioButton_rectangle.AutoSize = true;
            this.radioButton_rectangle.Location = new System.Drawing.Point(5, 102);
            this.radioButton_rectangle.Name = "radioButton_rectangle";
            this.radioButton_rectangle.Size = new System.Drawing.Size(93, 21);
            this.radioButton_rectangle.TabIndex = 10;
            this.radioButton_rectangle.TabStop = true;
            this.radioButton_rectangle.Text = "Rectangle";
            this.radioButton_rectangle.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(758, 394);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 25);
            this.label3.TabIndex = 13;
            this.label3.Text = "Thickness";
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Location = new System.Drawing.Point(759, 471);
            this.hScrollBar1.Maximum = 30;
            this.hScrollBar1.Minimum = 1;
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(178, 25);
            this.hScrollBar1.TabIndex = 14;
            this.hScrollBar1.Value = 1;
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            // 
            // lbVelicina
            // 
            this.lbVelicina.AutoSize = true;
            this.lbVelicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbVelicina.Location = new System.Drawing.Point(769, 440);
            this.lbVelicina.Name = "lbVelicina";
            this.lbVelicina.Size = new System.Drawing.Size(0, 20);
            this.lbVelicina.TabIndex = 15;
            // 
            // groupBox_shapes
            // 
            this.groupBox_shapes.Controls.Add(this.radioButton1_red);
            this.groupBox_shapes.Controls.Add(this.radioButton2_black);
            this.groupBox_shapes.Controls.Add(this.radioButton3_green);
            this.groupBox_shapes.Controls.Add(this.radioButton4_Blue);
            this.groupBox_shapes.Location = new System.Drawing.Point(759, 38);
            this.groupBox_shapes.Name = "groupBox_shapes";
            this.groupBox_shapes.Size = new System.Drawing.Size(143, 128);
            this.groupBox_shapes.TabIndex = 16;
            this.groupBox_shapes.TabStop = false;
            // 
            // groupBox_colors
            // 
            this.groupBox_colors.Controls.Add(this.radioButton5_line);
            this.groupBox_colors.Controls.Add(this.radioButton6_circle);
            this.groupBox_colors.Controls.Add(this.radioButton7_Triangle);
            this.groupBox_colors.Controls.Add(this.radioButton_rectangle);
            this.groupBox_colors.Location = new System.Drawing.Point(754, 225);
            this.groupBox_colors.Name = "groupBox_colors";
            this.groupBox_colors.Size = new System.Drawing.Size(178, 166);
            this.groupBox_colors.TabIndex = 17;
            this.groupBox_colors.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(946, 599);
            this.Controls.Add(this.groupBox_colors);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox_shapes);
            this.Controls.Add(this.lbVelicina);
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pbDrawing);
            this.Name = "Form1";
            this.Text = "Drawing";
            ((System.ComponentModel.ISupportInitialize)(this.pbDrawing)).EndInit();
            this.groupBox_shapes.ResumeLayout(false);
            this.groupBox_shapes.PerformLayout();
            this.groupBox_colors.ResumeLayout(false);
            this.groupBox_colors.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbDrawing;
        private System.Windows.Forms.RadioButton radioButton1_red;
        private System.Windows.Forms.RadioButton radioButton2_black;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton3_green;
        private System.Windows.Forms.RadioButton radioButton4_Blue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButton5_line;
        private System.Windows.Forms.RadioButton radioButton6_circle;
        private System.Windows.Forms.RadioButton radioButton7_Triangle;
        private System.Windows.Forms.RadioButton radioButton_rectangle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Label lbVelicina;
        private System.Windows.Forms.GroupBox groupBox_shapes;
        private System.Windows.Forms.GroupBox groupBox_colors;
    }
}

